//made by Cole Kuehmann This will make random integers and integer arrays 
//and either insert two arrays into each other or shorten an array by removing one index by user input

import java.util.Scanner;
import java.util.Random;
public class ArrayGames {
  public static void main(String[] args) {
    final int RANDOM_SHORTEN_INDEX_MAX = 25; //the maximum index that could be passed into the method shorten
    String usersInput;
    Scanner myScanner = new Scanner(System.in);
    //Below gets the users choice of insert or shorten
    do {
      System.out.print("Please enter the type of operation you want to do on the arrays (insert, shorten): ");
      usersInput=myScanner.next();
      if(!(usersInput.equals("insert")^usersInput.equals("shorten"))) {
        System.out.println("The only valid options are insert, or shorten.");
      }
    } while(!(usersInput.equals("insert")^usersInput.equals("shorten")));
    
    if(usersInput.equals("insert")) {
      //Below generates the two arrays to insert
      int[] arrayOne = generate();
      int[] arrayTwo = generate();
      
      //below prints out the two input arrays
      System.out.print("Input 1: ");
      print(arrayOne);
      System.out.print("Input 2: ");
      print(arrayTwo);
      
      //below assigns the inserted arrays and prints it as an output
      int[] insertedArray = insert(arrayOne,arrayTwo);
      System.out.print("Output: ");
      print(insertedArray);
    } else {
      //Below makes a random number generator and a random integer array
      int[] arrayOne = generate();
      Random myRNG = new Random();
      //below prints out the integer array and the index it randomly generated to remove
      System.out.print("Input 1: ");
      print(arrayOne);
      int reducedIndex= myRNG.nextInt(RANDOM_SHORTEN_INDEX_MAX+1);
      System.out.println("Input 2: "+reducedIndex);
      
      //below assigns the array returned from the shorten method and prints it as an output
      int[] shortenedArray = shorten(arrayOne,reducedIndex);
      System.out.print("Output: ");
      print(shortenedArray);
    }
  }
  
  public static int[] generate() {
    //below assigns the max int value the arrays could have
    final int MAX_INT_IN_ARRAYS = 25;
    Random myRNG = new Random();
    
    //sets up the int array
    int[] newArray = new int[myRNG.nextInt(10)+10];
    
    //below assigns random integer values in the array
    for(int currentIndex=0; currentIndex<newArray.length; currentIndex++) {
      newArray[currentIndex]=myRNG.nextInt(MAX_INT_IN_ARRAYS+1);
    }
    
    
    return newArray;
  }
  
  public static void print(int[] myArray) {
    
    //always prints the starting {
    System.out.print("{");
    //below iterates through the array printing the values then a , except for the last index
    for(int currentIndex=0;currentIndex<myArray.length-1;currentIndex++) {
      System.out.print(myArray[currentIndex]+",");
    }
    
    //prints the last index and adds a } as the last value should not have a comma after it
    System.out.println(myArray[myArray.length-1]+"}");
  }
  
  public static int[] shorten(int[] myArray,int indexToRemove) {
    //below checks if the given index to remove is out of bounds of the array
    if(indexToRemove>=myArray.length||indexToRemove<0) {
      return myArray;
    }
    
    //below creates the new array that will hold the old array except the index that will be removed
    int[] newArray = new int[myArray.length-1];
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      
      //below checks if it the index is 0-indexToRemove and assigns the values from myArray to newArray
      if(currentIndex<indexToRemove) {
         newArray[currentIndex]=myArray[currentIndex];
      } else {
        
        //below skips the index that we are removing
        if(currentIndex==indexToRemove) {
          continue;
        }
        
        //below assigns the values to newArray accounting for the skipped index
        newArray[currentIndex-1]=myArray[currentIndex];
      }
    }
    
    return newArray;
  }
  
  public static int[] insert(int[] arrayOne,int[] arrayTwo) {
    Random myRNG = new Random();
    //below creates a new array to hold both arrayOne and arrayTwo, along with the integer that we will be inserting after
    int[] newArray = new int[arrayOne.length+arrayTwo.length];
    int indexToInsertAt = myRNG.nextInt(arrayOne.length);
    
    System.out.println("Index inserting after: "+indexToInsertAt);
    
    //below iterates through all values in arrayOne and arrayTwo assigning each one to a correlating place in newArray given the index to insert after
    for(int currentIndex = 0;currentIndex<newArray.length;currentIndex++) {
      
      //below checks if it is before or at the index to insert afterwards and copies values from arrayOne
      if(currentIndex<=indexToInsertAt) {
        newArray[currentIndex]=arrayOne[currentIndex];
        
        //below checks if it has gone through all of array two as it is inserting arrayTwo after the index indexToInsertAt
      } else if(currentIndex<arrayTwo.length+indexToInsertAt+1) {
        newArray[currentIndex]=arrayTwo[currentIndex-indexToInsertAt-1];
        
        // else if both have of the above are false then we are left to reinsert the rest of arrayOne 
      } else {
        newArray[currentIndex]=arrayOne[currentIndex-arrayTwo.length];
      }
    }
    
    return newArray;
  }
  
  
}
