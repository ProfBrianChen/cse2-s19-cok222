//Below asks the user for a distance in meters and converts it to inches
import java.util.Scanner;

public class Convert {
  
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in);
    //Below asks for the distance in meters and stores it in a double
    System.out.print("Enter the distance in meters: ");
    double distanceInMeters=myScanner.nextDouble();
    
    //Saving the value in inches by taking the distance in meters and multiplying it by 39.37
    double distanceInInches=39.37*distanceInMeters;
    
    //Below prints the distance and formats the distance in inches to go to four decimal places
    System.out.println(distanceInMeters+" meters is "+String.format("%.4f", distanceInInches)+" in inches.");
  }
}