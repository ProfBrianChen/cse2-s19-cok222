//Below asks the user for the length width and height of a box and returns the boxes volume
import java.util.Scanner;

public class BoxVolume {
  
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in);
    double boxVolume=0;
    
    //Below gets the width of the box and sets it as the box volume. Later will multiply to volume to get volume
    System.out.print("The width side of the box is: ");
    boxVolume=myScanner.nextDouble();
    
    //Below gets the length of the box and multiplies it to the width
    System.out.print("The length of the box is: ");
    boxVolume*=myScanner.nextDouble();
    
    
    //Below gets the height of the box and multiplies it to width*length which will get the total volume
    System.out.print("The height of the box is: ");
    boxVolume*=myScanner.nextDouble();
    
    //Below prints a blank line for readability then returns the calculated volume to the user
    System.out.println();
    System.out.println("The volume inside the box is: "+boxVolume);
  }
}