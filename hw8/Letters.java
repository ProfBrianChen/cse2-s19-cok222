// Below generates a random array of chars and sorts them into two arrays which contain the chars AtoM and NtoZ and prints the original array and the sorted arrays
import java.util.Random;
public class Letters {
  public static void main(String[] args) {
    //Below sets the size of the random character array that will be sorted
    final int randomArraySize = 20;
    
    char[] myRandomArray = makeRandomArray(randomArraySize);
    
    //Below prints out the random character array
    System.out.print("Random character array: ");
    printArray(myRandomArray);
    System.out.println();
    
    //Below sorts the random array into two sections of the alphabet
    char[] sortedArrayAtoM= sortArray(myRandomArray,'a','m');
    char[] sortedArrayNtoZ= sortArray(myRandomArray,'n','z');
    
    //prints out the first sorted array
    System.out.print("AtoM characters: ");
    printArray(sortedArrayAtoM);
    System.out.println();
    
    //prints out the second sorted array
    System.out.print("NtoZ characters: ");
    printArray(sortedArrayNtoZ);
    System.out.println();
  }
  
  //below returns an array that is a copy of the input array and is extended by one
  public static char[] extendArray(char[] myArray) {
    char[] extendedArray = new char[myArray.length+1];
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      extendedArray[currentIndex] = myArray[currentIndex];
    }
    return extendedArray;
  }
  
  //below makes and returns an array with the size based on the given integer 
  public static char[] makeRandomArray(int size) {
    Random myRNG = new Random();
    char[] myArray = new char[size];
    for(int currentIndex = 0; currentIndex<size;currentIndex++) {
      
      //below gives a 50/50 chance for a char to be uppercase or lowercase. Uppercase being if myRNG.nextBoolean is true
      if(myRNG.nextBoolean()) {
        myArray[currentIndex]=(char)(myRNG.nextInt(26)+65);
      } else {
        myArray[currentIndex]=(char)(myRNG.nextInt(26)+97);
      }
      
    }
    return myArray;
  }
  
  public static char[] sortArray(char[] myArray, char startingChar, char endingChar) {
    char[] sortedArray = new char[0];
    for(int currentIndex = 0; currentIndex<myArray.length;currentIndex++) {
      
      //Below just checks if the char at currentIndex in myArray is between and including the lowercase and uppercase chars of the given startingChar and endingChar
      if((myArray[currentIndex]>=Character.toLowerCase(startingChar)&&myArray[currentIndex]<=Character.toLowerCase(endingChar))||(myArray[currentIndex]>=Character.toUpperCase(startingChar)&&myArray[currentIndex]<=Character.toUpperCase(endingChar))) {
        //below extends the array, sortedArray, by 1 as to make room for the newly found char between and including startingChar, endingChar.
        sortedArray= extendArray(sortedArray);
        //below assigns the newly found char to the sortedArray
        sortedArray[sortedArray.length-1] = myArray[currentIndex];
      }
    }
    return sortedArray;
  }
  
  //Just iterates through the array and prints out every char in the array
  public static void printArray(char[] myArray) {
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      System.out.print(myArray[currentIndex]);
    }
  }
}