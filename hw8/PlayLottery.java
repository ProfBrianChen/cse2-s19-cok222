//This class asks the user for 5 lottery numbers then compares it to 5 generated ones and sees if they match
//if they do then it says they won the lottery, if they didn't then they lose

import java.util.Random;
import java.util.Scanner;

public class PlayLottery {
  public static void main(String[] args) {
    int[] userNums = new int[5];
    Scanner myScanner =new Scanner(System.in);
    
    //Below gets the five lottery numbers from the user, making sure they are integers and between 0 and 59
    System.out.println("Please enter your lottery numbers: ");
    for(int currentNum=0; currentNum<userNums.length;currentNum++) {
      int lotteryNum=0;
      
      do {
        System.out.print("Lottery number "+(currentNum+1)+": ");
        while(!myScanner.hasNextInt()) {
          System.out.println("That is not an integer between 0 and 59.");
          String junk = myScanner.next();
          System.out.print("Please enter a positive integer for the lottery number: ");
        }
        lotteryNum=myScanner.nextInt();
        if(lotteryNum<0||lotteryNum>59) {
          System.out.println("Please enter an integer between 0 and 59");
        }
      } while(lotteryNum<0||lotteryNum>59);
      
      //Assigns the lottery number given by the user to the int array that stores the user supplied lottery numbers
      userNums[currentNum]=lotteryNum;
    }
    
    //below prints out the lottery numbers supplied by the user
    System.out.print("Your Lottery Numbers: "+userNums[0]);
    for(int currentIndex=1; currentIndex<userNums.length;currentIndex++) {
      System.out.print(", "+userNums[currentIndex]);
    }
    System.out.println();
    //below generates the winning lottery numbers
    int[] winningNums=numbersPicked();
    
    //below prints out the winning lottery numbers
    System.out.print("The Winning Lottery Numbers: "+winningNums[0]);
    for(int currentIndex=1; currentIndex<userNums.length;currentIndex++) {
      System.out.print(", "+winningNums[currentIndex]);
    }
    System.out.println();
    
    //Below just checks if the user won the lottery
    if(userWins(userNums,winningNums)) {
      System.out.println("You Won The Lottery!");
    } else {
      System.out.println("You lose.");
    }
  }
  
  //returns true when the integer arrays user and winning are the same.
  public static boolean userWins(int[] user, int[] winning) {
    for(int currentIndex=0; currentIndex<winning.length;currentIndex++) {
      
      //if any user number doesnt match the winning number then they lose and returns false
      if(user[currentIndex]!=winning[currentIndex]) {
        return false;
      }
    }
    
    //all user supplied numbers must have matched and returns true
    return true;
  }

  //generates the random numbers for the lottery without 
  //duplication.
  public static int[] numbersPicked() {
    Random myRNG = new Random();
    int[] winningNums = new int[5];
    
    for(int currentIndex = 0; currentIndex<winningNums.length; currentIndex++) {
      winningNums[currentIndex]=myRNG.nextInt(60);
      //Below checks if a generated number is the same as a previous one
      //and then retries by generating a new number for the same index by decrimenting currentIndex
      for(int testingIndex = 0; testingIndex<currentIndex;testingIndex++) {
        if(winningNums[testingIndex]==winningNums[currentIndex]) {
          currentIndex--;
          break;
        }
      }
    }
    
    return winningNums;
  }
  

} 
