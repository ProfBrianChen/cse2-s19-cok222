//This class creates sentences using randomly generated numbers to choose verbs, nouns, and adjectives
import java.util.Random;
import java.util.Scanner;
public class SentenceMaker {
  
  public static void main(String args[]) {
    generateParagraph();
  }
  /**
   *Calls the generateThesis method, generateSupportingSentence, and generateConclusion methods as to create a paragraph
   */
  public static void generateParagraph() {
    Random myRNG = new Random();
    String mySubject = generateThesis();
    String myObject;
    do{
      myObject = generateSupportingSentence(mySubject);
    } while(myRNG.nextInt(3)==0);
    generateConclusion(mySubject,myObject);
  }
  /**
   * Generated a random adjective out of a list of 10
   * @return returns the randomly selected adjective
   */
  public static String getAdjective() {
    String generatedAdjective="";
    Random myRNG = new Random();
    switch(myRNG.nextInt(10)) {
      case 0:
        generatedAdjective="lazy";
        break;
      case 1:
        generatedAdjective="bitter";
        break;
      case 2:
        generatedAdjective="slick";
        break;
      case 3:
        generatedAdjective="rough";
        break;
      case 4:
        generatedAdjective="grey";
        break;
      case 5:
        generatedAdjective="brown";
        break;
      case 6:
        generatedAdjective="prickly";
        break;
      case 7:
        generatedAdjective="small";
        break;
      case 8:
        generatedAdjective="epic";
        break;
      case 9:
        generatedAdjective="silly";
    }
    return generatedAdjective;
  }
  
    /**
     * Generated a random noun that fits as a subject out of a list of 10
     * @return returns the randomly selected noun
     */
  public static String getNounSubject() {
    String generatedNoun="";
    Random myRNG = new Random();
    switch(myRNG.nextInt(10)) {
        case 0:
        generatedNoun="fox";
        break;
      case 1:
        generatedNoun="dog";
        break;
      case 2:
        generatedNoun="human";
        break;
      case 3:
        generatedNoun="grasshopper";
        break;
      case 4:
        generatedNoun="cow";
        break;
      case 5:
        generatedNoun="kid";
        break;
      case 6:
        generatedNoun="fish";
        break;
      case 7:
        generatedNoun="whale";
        break;
      case 8:
        generatedNoun="horse";
        break;
      case 9:
        generatedNoun="pig";
    }
    return generatedNoun;
  }
  /**
     * Generated a random noun that fits as a object out of a list of 10
     * @return returns the randomly selected noun
     */
  public static String getNounObject() {
    String generatedNoun="";
    Random myRNG = new Random();
    switch(myRNG.nextInt(10)) {
        case 0:
        generatedNoun="jalapeno popper";
        break;
      case 1:
        generatedNoun="blade of grass";
        break;
      case 2:
        generatedNoun="airplane";
        break;
      case 3:
        generatedNoun="car";
        break;
      case 4:
        generatedNoun="burrito";
        break;
      case 5:
        generatedNoun="taco";
        break;
      case 6:
        generatedNoun="trash can";
        break;
      case 7:
        generatedNoun="light bulb";
        break;
      case 8:
        generatedNoun="skateboard";
        break;
      case 9:
        generatedNoun="computer";
    }
    return generatedNoun;
  }
  /**
     * Generated a random past tense verb out of a list of 10
     * @return returns the randomly selected past tense verb
     */
  public static String getVerb() {
    String generatedVerb="";
    Random myRNG = new Random();
    switch(myRNG.nextInt(10)) {
        case 0:
        generatedVerb="punched";
        break;
      case 1:
        generatedVerb="passed";
        break;
      case 2:
        generatedVerb="ignored";
        break;
      case 3:
        generatedVerb="visited";
        break;
      case 4:
        generatedVerb="threw";
        break;
      case 5:
        generatedVerb="ate";
        break;
      case 6:
        generatedVerb="built";
        break;
      case 7:
        generatedVerb="moved";
        break;
      case 8:
        generatedVerb="kicked";
        break;
      case 9:
        generatedVerb="touched";
    }
    return generatedVerb;
  }
  /**
   * prints a sentence using a random amount of adjectives describing the nouns in a subject verb object style sentence
   * @return returns the subject of the sentence
   */
  public static String generateThesis() {
    String currentSentence="the";
    Random myRNG = new Random();
    String thesisSubject;
    do{
      currentSentence+=" "+getAdjective();
    } while(myRNG.nextInt(5)==0);
    thesisSubject=getNounSubject();
    currentSentence+=" "+thesisSubject;
    currentSentence+=" "+getVerb();
    currentSentence+=" the";
    do{
      currentSentence+=" "+getAdjective();
    } while(myRNG.nextInt(5)==0);
    currentSentence+=" "+getNounObject();
    System.out.println(currentSentence+".");
    return thesisSubject;
  }
  /**
   * prints a sentence using a random amount of adjectives describing the nouns in a subject verb object style sentence
   * @return returns an object of the sentence
   */
  public static String generateSupportingSentence(String subject) {
    String currentSentence="";
    Random myRNG = new Random();
    if(myRNG.nextInt(2)==0) {
      currentSentence+="It ";
    } else {
      currentSentence+="The "+subject;
    }
    String finalObject="";
    if(myRNG.nextInt(2)==0) {
      String myVerb=getVerb();
      currentSentence+=" used a "+getNounObject();
      currentSentence+=" as it "+myVerb+" a";
      finalObject= myVerb +" the ";
    } else {
      String myVerb=getVerb();
      currentSentence+=" "+getVerb()+" a "+getNounObject()+" as it also "+myVerb+" a";
      finalObject= myVerb +" the ";
    }
    do{
      currentSentence+=" "+getAdjective();
    } while(myRNG.nextInt(5)==0);
    String myObject=getNounObject();
    finalObject+=myObject;
    currentSentence+=" "+myObject;
    System.out.println(currentSentence+".");
    return finalObject;
  }
  /**
   * prints a sentence using a random amount of adjectives describing the nouns in a subject verb object style sentence
   * @return returns the subject of the sentence
   */
  public static void generateConclusion(String subject,String object) {
    Random myRNG = new Random();
    String currentSentence;
    if(myRNG.nextInt(2)==0) {
      currentSentence="The "+subject+" really "+object+".";
    } else {
      currentSentence="The "+subject+" will always "+object+".";
    }
    System.out.println(currentSentence);
  }
}