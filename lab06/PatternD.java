/*Creates a pattern like below with the depth of whatever the user inputs. Example below is if the user input 6
  6 5 4 3 2 1
  5 4 3 2 1
  4 3 2 1
  3 2 1
  2 1
  1
*/
import java.util.Scanner;
public class PatternD {
  public static void main(String args[]) {
    //Below is the length of the pattern
    int patternLength=0;
    
    Scanner myScanner = new Scanner(System.in);
    
    //Below makes sure I get an integer from the user to set for the length of the pattern and assigns it to patternLength
     do {
      System.out.print("Please enter a positive integer for the length of the pattern: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive integer for the pattern length: ");
      }
      patternLength=myScanner.nextInt();
    } while(patternLength<1);
    
    for(int currentRow=patternLength;currentRow>=1;currentRow--) {
      for(int currentNumberToPrint=currentRow;currentNumberToPrint>=1; currentNumberToPrint--) {
        System.out.print(currentNumberToPrint+" ");
      }
      //Below enters a blank line to make a new row
      System.out.println();
    }
  }
}