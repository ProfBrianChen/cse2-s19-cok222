//This creates a twist with a length based on what the user entered. It has /\ alternating on top. \/ on bottom. and x in the middle

/* in example, here is the twist with length 25
 *  \ /\ /\ /\ /\ /\ /\ /\ /\
 *   X  X  X  X  X  X  X  X  
 *  / \/ \/ \/ \/ \/ \/ \/ \/
 *
 */
import java.util.Scanner;
public class TwistGenerator {
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in);
    int twistLength=0;
    System.out.print("Please enter an length for the twist: ");
    //Below Makes sure the user has given a int for length before trying to make the twist
    while(!myScanner.hasNextInt()) {
      if(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer, please enter an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter an length for the twist: ");
      }
    }
    twistLength=myScanner.nextInt();
    
    //Below goes for the length of the users input alternating "\" then " " then "/" 
    for(int i=0;i<twistLength;i++) {
      switch(i%3) {
        case 0:
           System.out.print("\\");
           break;
        case 1:
            System.out.print(" ");
            break;
        case 2:
            System.out.print("/");
        }
      }
    System.out.println();
      
      //Below goes for the length of the users input alternating " " then "X"
      for(int i=0;i<twistLength;i++) {
        switch(i%3) {
          case 0:
             System.out.print(" ");
             break;
          case 1:
              System.out.print("X");
              break;
          case 2:
            System.out.print(" ");
        }
      }
    System.out.println();
        
      //Below goes for the length of the users input alternating "/" then " " then "\" 
      for(int i=0;i<twistLength;i++) {
        switch(i%3) {
          case 0:
             System.out.print("/");
             break;
          case 1:
              System.out.print(" ");
              break;
          case 2:
              System.out.print("\\");
        }
      }
    System.out.println();
  }
}