//Given a string, it will ask the user if it wants to only check it for letter characters up to a certain length. if they don't it checks the entire string.
import java.util.Scanner;
public class StringAnalysis {
  public static void main(String args[]) {
    Scanner myScanner = new Scanner(System.in);
    String stringToCheck;
    char checkingFullLength;
    int indexTocheckTo;
    
    System.out.print("Please enter a string to check if it is all letter characters: ");
    stringToCheck=myScanner.next();
    
    //Below gets whether the user would like to only check a certain number of characters
    do {
        System.out.print("Would you like to check only a certain number of characters? (y/n): ");
        checkingFullLength=(myScanner.next()).charAt(0);
        if(!((Character.compare('y',(Character.toLowerCase(checkingFullLength)))!=0)^(Character.compare('n',(Character.toLowerCase(checkingFullLength)))!=0))) {
          System.out.println("You did not answer (y/n) or (yes/no).");
        }
      } while(!((Character.compare('y',(Character.toLowerCase(checkingFullLength)))!=0)^(Character.compare('n',(Character.toLowerCase(checkingFullLength)))!=0)));
  
    //Below gets a length to check to if the user would like to check to a specific length or otherwise sets it to the strings length
    if(Character.compare('y',(Character.toLowerCase(checkingFullLength)))==0) {
      do {
        System.out.print("Please enter a positive int for the length to check the string to: ");
        while(!myScanner.hasNextInt()) {
          System.out.println("That is not an int.");
          String junkWord = myScanner.next();
          System.out.print("Please enter a positive int for the the length to check the string to: ");
        }
        indexTocheckTo=myScanner.nextInt();
      } while(indexTocheckTo<1);
    } else {
      indexTocheckTo=0;
    }
    
    //below sorts it into whether it checks the entire string or just to a specific length then checks whether it contains only letters
    if(indexTocheckTo==0) {
      if(checkString(stringToCheck)) {
        System.out.println("The string contains only letters.");
      } else {
        System.out.println("The string contains characters other than just letters.");
      }
    } else {
      if(checkString(stringToCheck,indexTocheckTo-1)) {
        System.out.println("The string contains only letters up to the specified length.");
      } else {
        System.out.println("The string contains characters other than just letters within the specified length.");
      }
    }
  }
  
  /* Checks the string to make sure the input string is all alphabetical characters
   * @param stringToCheck The string that this method checks
   * @return returns true if the string is valid up to the specified indice.
   */
  public static boolean checkString(String stringToCheck) {
    for(int currentIndex=0; currentIndex<stringToCheck.length();currentIndex++) {
      if(!(Character.toLowerCase(stringToCheck.charAt(currentIndex))>='a'&&Character.toLowerCase(stringToCheck.charAt(currentIndex))<='z'))
        return false;
    }
    return true;
  }
  
  /* Checks the string to make sure the input string is all alphabetical characters up to the length input
   * @param stringToCheck The string that this method checks
   * @param lengthToCheckTo The max index that you would like to check to
   * @return returns true if the string is valid up to the specified indice.
   */
  public static boolean checkString(String stringToCheck, int lengthToCheckTo) {
    for(int currentIndex=0; currentIndex<=lengthToCheckTo;currentIndex++) {
      if(!(currentIndex>=stringToCheck.length())&&!(Character.toLowerCase(stringToCheck.charAt(currentIndex))>='a'&&Character.toLowerCase(stringToCheck.charAt(currentIndex))<='z')) {
        return false;
      }
    }
    return true;
  }
}