//Below calculates the area of a circle, rectangle, or triangle depending on what the user inputs the shape as.
import java.util.Scanner;
public class Area {
  public static void main(String args[]) {
    double area;
    switch(getShape()){
      case 0:
        area = circleArea(getValidMeasurementInput("radius"));
        System.out.println("The area of the circle is "+area);
        break;
      case 1:
        area = rectangleArea(getValidMeasurementInput("height"),getValidMeasurementInput("width"));
        System.out.println("The area of the rectangle is "+ area);
        break;
      case 2:
        area = triangleArea(getValidMeasurementInput("height"),getValidMeasurementInput("width"));
        System.out.println("The area of the triangle is "+ area);
    }
  }
  
/**
 * Gets a valid double from the user given a measurement name
 * @param measurementName The name of the measurement to ask the user for (i.e. radius, width, or height)
 * @return The double entered by the user
 */
  public static double getValidMeasurementInput(String measurementName) {
    Scanner myScanner = new Scanner(System.in);
    double currentSize;
    do {
      System.out.print("Please enter a positive double for the "+measurementName+": ");
      while(!myScanner.hasNextDouble()) {
        System.out.println("That is not an double.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive double for the "+measurementName+": ");
      }
      currentSize=myScanner.nextDouble();
    } while(currentSize<1);
    return currentSize;
  }
  
  /**
   * Gets the shape the user wants between circle rectangle and triangle and returns a corresponding int
   * @return returns 0 if user selected circle. 1 if user selected rectangle. 2 if the user selected triangle. -1 if there is an error
   */
  public static int getShape() {
    Scanner myScanner = new Scanner(System.in);
    String usersInput;
    do {
        System.out.print("Please enter the shape that you want to find the volume of (circle, rectangle, triangle): ");
        usersInput=myScanner.next();
        if(!(usersInput.equals("circle")^usersInput.equals("rectangle")^usersInput.equals("triangle"))) {
          System.out.println("The only valid options are circle, rectangle, and triangle.");
        }
      } while(!(usersInput.equals("circle")^usersInput.equals("rectangle")^usersInput.equals("triangle")));
    if(usersInput.equals("circle")) {
      return 0;
    } else if(usersInput.equals("rectangle")) {
      return 1;
    } else if(usersInput.equals("triangle")) {
      return 2;
    }
    return -1;
  }
  
/**
 * returns the area of a circle if given a radius
 * @param radius the radius of the circle
 * @return the calculated area
 */
  public static double circleArea(double radius) {
    return Math.PI*radius*radius;
  }
  
  /**
   * returns the area of a rectangle when given a height and width
   * @param height the height of the rectangle
   * @param width the width of the rectangle
   * @return the calculated area
   */
  public static double rectangleArea(double height, double width) {
    return height*width;
  }
  
  /**
   * returns the area of a triangle when given a height and width
   * @param height the height of the triangle
   * @param width the width of the triangle
   * @return the calculated area
   */
  public static double triangleArea(double height, double width) {
    return 0.5*height*width;
  }
}