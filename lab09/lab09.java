import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;
public class lab09 {
  public static void main(String[] args) {
    //initializes all my required variables
    Scanner myScanner = new Scanner(System.in);
    String usersInput;
    int arraySize;
    int arrayKey;
    int[] myArray = new int[0];
    
    //Below gets the search type
    do {
      System.out.print("Please enter the type of search you want to do(linear,binary): ");
      usersInput=myScanner.next();
      if(!(usersInput.equals("linear")^usersInput.equals("binary"))) {
        System.out.println("The only valid options are linear, or binary.");
      }
    } while(!(usersInput.equals("linear")^usersInput.equals("binary")));
    
    do {
      System.out.print("Input your desired array size: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junk = myScanner.next();
        System.out.print("Please enter a positive integer: ");
      }
      arraySize=myScanner.nextInt();
    } while(arraySize<1);
    
    //below generates and prints the array
    if(usersInput.equals("linear")) {
      myArray=generateUnsortedArray(arraySize);
      printArray(myArray);
    } else if(usersInput.equals("binary")) {
      myArray=generateSortedArray(arraySize);
      printArray(myArray);
    }
    
    //Below gets the int to search for
    System.out.print("Input your desired integer to search for: ");
    while(!myScanner.hasNextInt()) {
      System.out.println("That is not an integer.");
      String junk = myScanner.next();
      System.out.print("Please enter an integer between 0 and "+arraySize+": ");
    }
    arrayKey=myScanner.nextInt();
    
    //below searches for the integer and prints an appropriate statement depending on if the integer is in the array
    if(usersInput.equals("linear")) {
      int indexFound=linearSearch(myArray,arrayKey);
      if(indexFound!=-1) {
        System.out.println("Found the integer at index: "+indexFound);
      } else {
        System.out.println("The integer was not found in the array.");
      }
    } else if(usersInput.equals("binary")) {
      int indexFound=binarySearch(myArray,arrayKey);
      if(indexFound!=-1) {
        System.out.println("Found the integer at index: "+indexFound);
      } else {
        System.out.println("The integer was not found in the array.");
      }
    }
  }
  
  //Below runs a binary search and finds the index where the key is. returns the index key is found at, or -1 if it is not in the array
  public static int binarySearch(int[] myArray,int key) {
    int min=0;
    int max=myArray.length-1;
    while(myArray[(max-min)/2+min]!=key) {
      //Checks if I have accidentally gone too far and returns an approprite value
      if(max<=min) {
        if(myArray[min]==key) {
          return min;
        } else {
          return -1;
        }
      }
      
      //If the middle value is too big then changes the max index the key could be at
      if(myArray[(max-min)/2+min]>key) {
        max=(max-min)/2+min-1;
      } else if(myArray[(max-min)/2+min]<key) { // if the middle value is too small then changes the min index the key could be at
        min=(max-min)/2+min+1;
      } else if(myArray[(max-min)/2+min]==key) { // index value matches key, returns the index
        return (max-min)/2+min;
      }
    }
    return (max-min)/2+min;
  }
  
  //Checks if any of the values match the key, if none do then returns -1 otherwise returns the index it found it at
  public static int linearSearch(int[] myArray,int key) {
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      if(myArray[currentIndex]==key) {
        return currentIndex;
      }
    }
    return -1;
  }
  
  //Prints out the array
  public static void printArray(int[] myArray) {
    System.out.print("The array = { "+myArray[0]);
    for(int currentIndex=1;currentIndex<myArray.length;currentIndex++) {
      System.out.print(", "+myArray[currentIndex]);
    }
    System.out.println("}");
  }
  
  //Generates an array with random values from 0 to arraySize
  public static int[] generateUnsortedArray(int arraySize) {
    Random myRNG = new Random();
    int[] myArray = new int[arraySize];
    for(int currentIndex=0;currentIndex<arraySize;currentIndex++) {
      myArray[currentIndex]=myRNG.nextInt(arraySize+1);
    }
    return myArray;
  }
  
  //Generates a unsortedArray then sorts them and returns that sorted array in accending order
  public static int[] generateSortedArray(int arraySize) {
    int[] myArray = generateUnsortedArray(arraySize);
    Arrays.sort(myArray);
    return myArray;
  }
}