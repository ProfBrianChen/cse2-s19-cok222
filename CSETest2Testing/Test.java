public class Test {
  public static void main(String[] args) {
    int x=7;
    for(int i=x;i>1;i-=2) {
      for (int j=0;j<x;j++) {
        if(j<i&&j>(x-i-1)) {
          System.out.print("#");
        } else {
          System.out.print(" ");
        }
      }
      System.out.println();
    }
    int k;
    if(x%2==0) {
      k=0;
    } else {
      k=1;
    }
    for(int i=k;i<=x;i+=2) {
      for(int j=x-1;j<=0;j--) {
        if(j<i&&j>(x-i-1)) {
          System.out.print("@");
        } else {
          System.out.print(" ");
        }
      }
      System.out.println();
    }
  }
}