HW5 Rubric

Total Score: 100

  75/75: Program compiles

  25/25: At least 5 of these are correct:
    course number,
    department name,
    the number of times it meets in a week,
    the time the class starts,
    the instructor name,
    and the number of students
