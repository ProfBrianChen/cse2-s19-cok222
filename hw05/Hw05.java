//User enters the department, course number, number of times it meets a week, instructor name, number of students, and time class starts
//It then verifies that these are all valid inputs
import java.util.Scanner;
public class Hw05 {
  public static void main(String args[]) {
    int courseNumber;
    int timesClassMeets;
    int numberOfStudents;
    double timeClassStarts;
    String department;
    String instructorName;
    
    Scanner myScanner = new Scanner(System.in);
    
    //Below makes sure it isn't a integer or double then takes in the input as the department String
    System.out.print("Please enter the department: ");
    while(myScanner.hasNextInt()||myScanner.hasNextDouble()) {
      if(myScanner.hasNextInt()||myScanner.hasNextDouble()) {
        System.out.println("That is not a String, please enter an String.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a String for the Department: ");
      }
    }
    department=myScanner.nextLine();
    
    //Below makes sure it isn't a integer or double then takes in the input as the instructors name String
    System.out.print("Please enter the instructors name: ");
    while(myScanner.hasNextInt()||myScanner.hasNextDouble()) {
      if(myScanner.hasNextInt()||myScanner.hasNextDouble()) {
        System.out.println("That is not a String, please enter an String.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a String for the instructors name: ");
      }
    }
    instructorName=myScanner.nextLine();
    
    //Below makes sure it is a integer for the course number
    System.out.print("Please enter the course number: ");
    while(!myScanner.hasNextInt()) {
      if(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer, please enter an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a integer for the course number: ");
      }
    }
    courseNumber=myScanner.nextInt();
    
    //Below makes sure it is a integer for the course number
    System.out.print("Please enter the number of times the course meets a week: ");
    while(!myScanner.hasNextInt()) {
      if(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer, please enter an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a integer for the amount of time class meets: ");
      }
    }
    timesClassMeets=myScanner.nextInt();
    
    //Below makes sure it is a integer for the course number
    System.out.print("Please enter the number of students in the class: ");
    while(!myScanner.hasNextInt()) {
      if(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer, please enter an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a integer for the amount of students in the class: ");
      }
    }
    numberOfStudents=myScanner.nextInt();
    
    //Below gets a double in the form of HH.MM where H is the hour and M is the minute for the start of the class
    System.out.print("Please enter the time the class starts in the form HH.MM: ");
    while(!myScanner.hasNextDouble()) {
      if(!myScanner.hasNextDouble()) {
        System.out.println("That is not a double, please enter an double.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a double for the time class starts in the form HH.MM: ");
      }
    }
    timeClassStarts=myScanner.nextDouble();
    
    //Below prints out all the info the user put in
    System.out.println("Department: "+department+"\nInstructors name: "+instructorName+"\nNumber of students: "+numberOfStudents+
                      "\nNumber of times it meets: "+timesClassMeets+"\nTime class starts: "+String.format("%.2f",timeClassStarts)+
                      "\nCourse number: "+String.format("%03d",courseNumber));
  }
}