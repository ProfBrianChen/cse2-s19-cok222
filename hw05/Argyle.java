//creates an argyle pattern given a users input for diamond width, width of center stripe, height, width, and characters to make it out of

import java.util.Scanner;
public class Argyle {
  
  public static void main (String args[]) {
    Scanner myScanner = new Scanner(System.in);
    
    
    //Below declares the input variables given by the user
    char firstPatternFill;
    char secondPatternFill;
    char stripeFill;
    int height;
    int width;
    int stripeWidth;
    int diamondWidth;
    
    //Below makes sure I get an integer from the user to set for the length of the pattern and assigns it to patternLength
    //The do while loops makes sure it is a positive integer
     do {
      System.out.print("please enter a positive integer for the width of Viewing window in characters: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive integer for width of the Viewing window: ");
      }
      width=myScanner.nextInt();
    } while(width<1);
    
    do {
      System.out.print("please enter a positive integer for the height of Viewing window in characters: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive integer for height of the Viewing window: ");
      }
      height=myScanner.nextInt();
    } while(height<1);
    
    do {
      System.out.print("please enter a positive integer for the width of the argyle diamonds: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive integer for the width of the argyle diamonds: ");
      }
      diamondWidth=myScanner.nextInt();
    } while(diamondWidth<1);
    
    //Below asks until the stripe width input is an odd integer and less than half of the width of the diamonds on top of the normal positive integer requierments
    do {
      System.out.print("please enter a positive odd integer for the width of the argyle center stripe: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junkWord = myScanner.next();
        System.out.print("Please enter a positive integer for the width of the argyle center stripe: ");
      }
      stripeWidth=myScanner.nextInt();
      if(!((double)stripeWidth<((double)diamondWidth/2.0))) {
        System.out.println("The stripe width needs to be less than half of the diamond width");
      } else if(!(stripeWidth%2==1)) {
        System.out.println("The stripe needs to be an odd integer!");
      }
    } while(stripeWidth<1||!(stripeWidth%2==1)||!((double)stripeWidth<(double)diamondWidth/2.0));
    
    
    //Below just gets the first character the user inputs
    System.out.print("please enter a first character for the pattern fill: ");
    String temp=myScanner.next();
    firstPatternFill=temp.charAt(0);
    
    System.out.print("please enter a second character for the pattern fill: ");
    temp=myScanner.next();
    secondPatternFill=temp.charAt(0);
    
    System.out.print("please enter a third character for the stripe fill: ");
    temp=myScanner.next();
    stripeFill=temp.charAt(0);
    
    //Below starts the for loops to iterate left to right, top to bottom of the width and height of the pattern
    for(int currentRow=0;currentRow<height;currentRow++) {
      for(int currentColumn=0;currentColumn<width;currentColumn++) {
        
        //The switch statement alternates 0 and 1 for left and right side, and then addes 0 or 2 alternating if it is the top or bottom
        //This allows us to split the pattern into four pieces. top left =0, top right =1, bottom left =2, bottom right =3.
        switch(((currentColumn/diamondWidth)%2)+(((currentRow/diamondWidth)%2)*2)) {
          case 0:
            //Below sees if it is along the top left to bottom right diagonal and also allows for the right and left of half the width of the stripe
            //Effectively making a stripe with full width 
            if(((currentColumn%diamondWidth)-(currentRow%diamondWidth))<(stripeWidth/2)+1&&((currentColumn%diamondWidth)-(currentRow%diamondWidth))>(-1*stripeWidth/2)-1) {
              System.out.print(stripeFill);
              
            //Below sees if the current column is larger than the width of the diamond minus the row effectivly making a traingle with the right angle
            //in the bottom right of the sub square
            //i.e using plus as the diamond, and diamond width 3
            /* ...
             * ..+
             * .++
             */
            } else if(currentColumn%diamondWidth>(diamondWidth-(currentRow%diamondWidth))-1){
              System.out.print(secondPatternFill);
              
            //if neither the above are true then it is just filler.(not a part of the stripe or diamond)
            } else {
              System.out.print(firstPatternFill);
            }
            break;
            
          case 1:
            //Below sees if it is along the top right to bottom left diagonal and also allows for the right and left of half the width of the stripe
            //Effectively making a stripe with full width 
            if(((currentColumn%diamondWidth)-(diamondWidth-(currentRow%diamondWidth)))<(stripeWidth/2)+1&&((currentColumn%diamondWidth)-(diamondWidth-(currentRow%diamondWidth)))>(-1*stripeWidth/2)-1) {
              System.out.print(stripeFill);
              
            //Below sees if the current column is smaller than the current row effectivly making a traingle with the right angle
            //in the bottom left of the sub square
            //i.e using plus as the diamond, and diamond width 3
            /* ...
             * +..
             * ++.
             */
            } else if(currentColumn%diamondWidth<currentRow%diamondWidth){
              System.out.print(secondPatternFill);
              
            //if neither the above are true then it is just filler.(not a part of the stripe or diamond)
            } else {
              System.out.print(firstPatternFill);
            }
            break;
            
          case 2:
            //Below sees if it is along the top right to bottom left diagonal and also allows to the right and left of half the width of the stripe
            //Effectively making a stripe with full width 
            if(((currentColumn%diamondWidth)-(diamondWidth-(currentRow%diamondWidth)))<(stripeWidth/2)+1&&((currentColumn%diamondWidth)-(diamondWidth-(currentRow%diamondWidth)))>(-1*stripeWidth/2)-1) {
              System.out.print(stripeFill);
              
            //Below sees if the current column is larger than the current row effectivly making a traingle with the right angle
            //in the top right of the sub square, subtracting 1 to fix the fencepost error of not reaching full diamond width
            //i.e using plus as the diamond, and diamond width 3
            /* +++
             * .++
             * ..+
             */
            } else if(currentColumn%diamondWidth>currentRow%diamondWidth-1) {
              System.out.print(secondPatternFill);
              
            //if neither the above are true then it is just filler.(not a part of the stripe or diamond)
            } else {
              System.out.print(firstPatternFill);
            }
            break;
            
          case 3:
            //Below sees if it is along the top left to bottom right diagonal and also allows for the right and left of half the width of the stripe
            //Effectively making a stripe with full width 
            if(((currentColumn%diamondWidth)-(currentRow%diamondWidth))<(stripeWidth/2)+1&&((currentColumn%diamondWidth)-(currentRow%diamondWidth))>(-1*stripeWidth/2)-1) {
              System.out.print(stripeFill);
              
            //Below sees if the current column is smaller than the width of the diamond minus the row effectivly making a traingle with the right angle
            //in the top left of the sub square
            //i.e using plus as the diamond, and diamond width 3
            /* +++
             * ++.
             * +..
             */
            } else if(currentColumn%diamondWidth<(diamondWidth-(currentRow%diamondWidth))) {
              System.out.print(secondPatternFill);
              
            //if neither the above are true then it is just filler.(not a part of the stripe or diamond)
            } else {
              System.out.print(firstPatternFill);
            }
        }
      }
      //Starts a new row
      System.out.println();
    }
  }
}