import java.util.Arrays;
import java.util.Random;
public class lab08 {
  public static void main(String[] args) {
    Random myRNG = new Random();
    int arraySize=50+myRNG.nextInt(51);
    int[] myArray = new int[arraySize];
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      myArray[currentIndex]= myRNG.nextInt(100);
      System.out.println("myArray["+currentIndex+"]: "+myArray[currentIndex]);
    }
    System.out.println("Array size = "+arraySize);
    System.out.println("Range = "+getRange(myArray));
    System.out.println("Mean = "+getMean(myArray));
    System.out.println("Standard Deviation = "+getStdDev(myArray));
    shuffle(myArray);
  }
  
  public static int getRange(int[] myArray) {
    Arrays.sort(myArray);
    return myArray[myArray.length-1]-myArray[0];
  }
  
  public static double getMean(int[] myArray) {
    int currentSum=0;
    for(int currentVal:myArray) {
      currentSum+=currentVal;
    }
    return currentSum/myArray.length;
  }
  
  public static double getStdDev(int[] myArray) {
    int sumOfDeviation=0;
    double mean=getMean(myArray);
    for(int currentVal:myArray) {
      sumOfDeviation+=(currentVal-mean)*(currentVal-mean);
    }
    double deviation=sumOfDeviation/(myArray.length-2);
    deviation=Math.sqrt(deviation);
    return deviation;
  }
  
  public static void shuffle(int[] myArray) {
    Random myRNG = new Random();
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      int temp = myArray[currentIndex];
      int randomIndex= myRNG.nextInt(myArray.length);
      myArray[currentIndex] = myArray[randomIndex];
      myArray[randomIndex]=temp;
    }
    System.out.println("Shuffled Array: ");
    for(int currentIndex=0;currentIndex<myArray.length;currentIndex++) {
      System.out.println("myArray["+currentIndex+"]: "+myArray[currentIndex]);
    }
  }
}