// Below takes in time in seconds, cycles of a bike wheel, and outputs total distance, and the average mph that you biked at
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  //The Amount of time Trip 1 lasted in seconds
      int secsTrip2=3220;  //The Amount of time Trip 2 lasted in seconds
		  int countsTrip1=1561;  //The amount of wheel rotations in trip 1
      int countsTrip2=9037; //The amount of wheel rotations in trip 2
      
      double wheelDiameter=27.0,  //The wheel diameter so to calculate distance using number of rotations
  	  PI=3.14159, //setting aproximated value of pi for circle calculations
  	  feetPerMile=5280,  //Setting constant value of feet per mile
  	  inchesPerFoot=12,   //Setting constant value of inches per a foot
  	  secondsPerMinute=60;  //Setting constant value of seconds in a minute
	    double distanceTrip1, distanceTrip2,totalDistance;  //Variables used to store the distances we calculate using above values
      
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //calculates distance in miles for trip 2
    	totalDistance=distanceTrip1+distanceTrip2; // calculates total distance using trip 1 and 2s distance
      
      //Print out the output data we calculated above.
      System.out.println("Trip 1 was "+distanceTrip1%0.4f+" miles");
    	System.out.println("Trip 2 was "+distanceTrip2%0.4f+" miles");
	    System.out.println("The total distance was "+totalDistance%0.4f+" miles");

	}  //end of main method   
} //end of class 
