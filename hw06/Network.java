//Below takes in a size to print the pattern, square size, and connecting edge length and prints out a series of connected squares
//testing atom as an editor
import java.util.Scanner;
public class Network {
  public static void main(String args[]) {
    Scanner myScanner =new Scanner(System.in);

    //Below initializes the users input variables
    int patternHeight;
    int patternWidth;
    int squareSize;
    int edgeLength;

    //Below makes sure to get a positive integer input for the height,width,square size, and edge length of the network pattern.
    do {
      System.out.print("Input your desired height: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junk = myScanner.next();
        System.out.print("Please enter a positive integer for the height: ");
      }
      patternHeight=myScanner.nextInt();
    } while(patternHeight<1);

    do {
      System.out.print("Input your desired width: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junk = myScanner.next();
        System.out.print("Please enter a positive integer for the width: ");
      }
      patternWidth=myScanner.nextInt();
    } while(patternWidth<1);

    do {
      System.out.print("Input square size: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junk = myScanner.next();
        System.out.print("Please enter a positive integer for the square size: ");
      }
      squareSize=myScanner.nextInt();
    } while(squareSize<1);

    do {
      System.out.print("Input edge length: ");
      while(!myScanner.hasNextInt()) {
        System.out.println("That is not an integer.");
        String junk = myScanner.next();
        System.out.print("Please enter a positive integer for the edge length: ");
      }
      edgeLength=myScanner.nextInt();
    } while(edgeLength<1);

    //Below loops goes from left to right, then top to bottom printing the squares, edges, and spaces
    for(int currentRow=0;currentRow<patternHeight;currentRow++) {
      for(int currentColumn=0;currentColumn<patternWidth;currentColumn++) {

        //Below checks if it is an edge to the right of the square
        if((currentColumn%(squareSize+edgeLength))/(squareSize)>0) {

          //Below sorts it into whether the square has an even or odd length then prints the edge based on that
          if(squareSize%2==0) {

            //Below checks if it is a row halfway through a square and the row below it as the square is an even length
            if((squareSize/2==(currentRow+1)%(squareSize+edgeLength))||((squareSize/2)+1==((currentRow+1)%(squareSize+edgeLength)))) {
              System.out.print("-");
            } else {
              System.out.print(" ");
            }

           } else {

            //Only else if it is an edge and an odd sized square, and checks if it is halfway down the square to see if it should print an edge
            if((squareSize/2)+1==(currentRow+1)%(squareSize+edgeLength)) {
              System.out.print("-");
            } else {
              System.out.print(" ");
            }
          }

        //Below checks if we are below the square and should make edges since we are not to the right of the square
        } else if((currentRow%(squareSize+edgeLength))/(squareSize)>0) {

          //Below sorts the square into whether it has an even length or odd length sides
          if(squareSize%2==0) {

            //Below checks if it is a column halfway through a square and the column after as the square is an even length to see if it should print a |
            if((squareSize/2==(currentColumn+1)%(squareSize+edgeLength))||((squareSize/2)+1==((currentColumn+1)%(squareSize+edgeLength)))) {
              System.out.print("|");
            } else {
              System.out.print(" ");
            }
          } else {

            //Only else if it is an edge and an odd sized square, and checks if it is halfway horizonstal to the square to see if it should print an edge
            if((squareSize/2)+1==(currentColumn+1)%(squareSize+edgeLength)) {
              System.out.print("|");
            } else {
              System.out.print(" ");
            }
          }

        //Below checks if on any of the four corner (hence four or statements) to put a # since we are not making an edge
        } else if((currentRow%(squareSize+edgeLength)==0&&currentColumn%(squareSize+edgeLength)==0)||(currentColumn%(squareSize+edgeLength)==squareSize-1&&(currentRow%(squareSize+edgeLength)==0))
                 ||(currentRow%(squareSize+edgeLength)==squareSize-1&&(currentColumn%(squareSize+edgeLength)==0))||(currentColumn%(squareSize+edgeLength)==squareSize-1&&(currentRow%(squareSize+edgeLength)==squareSize-1))) {

          System.out.print("#");

        //Below checks if we are on a top or bottom of the square and prints -
        } else if(currentRow%(squareSize+edgeLength)==0||(currentRow%(squareSize+edgeLength)==squareSize-1)) {
          System.out.print("-");

        //Below checks if we are on a left or right side of the square and print |
        } else if(currentColumn%(squareSize+edgeLength)==0||(currentColumn%(squareSize+edgeLength)==squareSize-1)) {
          System.out.print("|");

          //If none of the above are true, then it there suppose to be nothing and so we print a space
        } else {
          System.out.print(" ");
        }
      }
      System.out.println();
    }
  }
}
