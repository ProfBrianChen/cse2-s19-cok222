//This class generates 4 cards for a poker hand and tells you whether you have a pair, 2 pair, three of a kind or only a high card

public class PokerHandCheck{
  
  public static void main(String args[]) {
    //Below generates 5 cards using 5 different decks
    int myCard1=(int)(Math.random()*52)+1;
    int myCard2=(int)(Math.random()*52)+1;
    int myCard3=(int)(Math.random()*52)+1;
    int myCard4=(int)(Math.random()*52)+1;
    int myCard5=(int)(Math.random()*52)+1;
    //Below initializes the counts for counting what cards I have. Will be added if found while printing out the names of what cards you have
    int numberOfAces=0;
    int numberOf2s=0;
    int numberOf3s=0;
    int numberOf4s=0;
    int numberOf5s=0;
    int numberOf6s=0;
    int numberOf7s=0;
    int numberOf8s=0;
    int numberOf9s=0;
    int numberOf10s=0;
    int numberOfJacks=0;
    int numberOfQueens=0;
    int numberOfKings=0;
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "The Queen of"
    switch(myCard1%13) {
      case 0: 
        System.out.print("The King of ");
        numberOfKings++;
        break;
      case 1:
        System.out.print("The Ace of ");
        numberOfAces++;
        break;
      case 11:
        System.out.print("The Jack of ");
        numberOfJacks++;
        break;
      case 12:
        System.out.print("The Queen of ");
        numberOfQueens++;
        break;
      case 2:
        System.out.print("The 2 of ");
        numberOf2s++;
        break;
      case 3:
        System.out.print("The 3 of ");
        numberOf3s++;
        break;
      case 4:
        System.out.print("The 4 of ");
        numberOf4s++;
        break;
      case 5:
        System.out.print("The 5 of ");
        numberOf5s++;
        break;
      case 6:
        System.out.print("The 6 of ");
        numberOf6s++;
        break;
      case 7:
        System.out.print("The 7 of ");
        numberOf7s++;
        break;
      case 8:
        System.out.print("The 8 of ");
        numberOf8s++;
        break;
      case 9:
        System.out.print("The 9 of ");
        numberOf9s++;
        break;
      case 10:
        System.out.print("The 10 of ");
        numberOf10s++;
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myCard1/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "The Queen of"
    switch(myCard2%13) {
      case 0: 
        System.out.print("The King of ");
        break;
      case 1:
        System.out.print("The Ace of ");
        break;
      case 11:
        System.out.print("The Jack of ");
        break;
      case 12:
        System.out.print("The Queen of ");
        break;
      case 2:
        System.out.print("The 2 of ");
        numberOf2s++;
        break;
      case 3:
        System.out.print("The 3 of ");
        numberOf3s++;
        break;
      case 4:
        System.out.print("The 4 of ");
        numberOf4s++;
        break;
      case 5:
        System.out.print("The 5 of ");
        numberOf5s++;
        break;
      case 6:
        System.out.print("The 6 of ");
        numberOf6s++;
        break;
      case 7:
        System.out.print("The 7 of ");
        numberOf7s++;
        break;
      case 8:
        System.out.print("The 8 of ");
        numberOf8s++;
        break;
      case 9:
        System.out.print("The 9 of ");
        numberOf9s++;
        break;
      case 10:
        System.out.print("The 10 of ");
        numberOf10s++;
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myCard2/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "The Queen of"
    switch(myCard3%13) {
      case 0: 
        System.out.print("The King of ");
        break;
      case 1:
        System.out.print("The Ace of ");
        break;
      case 11:
        System.out.print("The Jack of ");
        break;
      case 12:
        System.out.print("The Queen of ");
        break;
      case 2:
        System.out.print("The 2 of ");
        numberOf2s++;
        break;
      case 3:
        System.out.print("The 3 of ");
        numberOf3s++;
        break;
      case 4:
        System.out.print("The 4 of ");
        numberOf4s++;
        break;
      case 5:
        System.out.print("The 5 of ");
        numberOf5s++;
        break;
      case 6:
        System.out.print("The 6 of ");
        numberOf6s++;
        break;
      case 7:
        System.out.print("The 7 of ");
        numberOf7s++;
        break;
      case 8:
        System.out.print("The 8 of ");
        numberOf8s++;
        break;
      case 9:
        System.out.print("The 9 of ");
        numberOf9s++;
        break;
      case 10:
        System.out.print("The 10 of ");
        numberOf10s++;
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myCard3/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "The Queen of"
    switch(myCard4%13) {
      case 0: 
        System.out.print("The King of ");
        break;
      case 1:
        System.out.print("The Ace of ");
        break;
      case 11:
        System.out.print("The Jack of ");
        break;
      case 12:
        System.out.print("The Queen of ");
        break;
      case 2:
        System.out.print("The 2 of ");
        numberOf2s++;
        break;
      case 3:
        System.out.print("The 3 of ");
        numberOf3s++;
        break;
      case 4:
        System.out.print("The 4 of ");
        numberOf4s++;
        break;
      case 5:
        System.out.print("The 5 of ");
        numberOf5s++;
        break;
      case 6:
        System.out.print("The 6 of ");
        numberOf6s++;
        break;
      case 7:
        System.out.print("The 7 of ");
        numberOf7s++;
        break;
      case 8:
        System.out.print("The 8 of ");
        numberOf8s++;
        break;
      case 9:
        System.out.print("The 9 of ");
        numberOf9s++;
        break;
      case 10:
        System.out.print("The 10 of ");
        numberOf10s++;
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myCard4/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "The Queen of"
    switch(myCard5%13) {
      case 0: 
        System.out.print("The King of ");
        break;
      case 1:
        System.out.print("The Ace of ");
        break;
      case 11:
        System.out.print("The Jack of ");
        break;
      case 12:
        System.out.print("The Queen of ");
        break;
      case 2:
        System.out.print("The 2 of ");
        numberOf2s++;
        break;
      case 3:
        System.out.print("The 3 of ");
        numberOf3s++;
        break;
      case 4:
        System.out.print("The 4 of ");
        numberOf4s++;
        break;
      case 5:
        System.out.print("The 5 of ");
        numberOf5s++;
        break;
      case 6:
        System.out.print("The 6 of ");
        numberOf6s++;
        break;
      case 7:
        System.out.print("The 7 of ");
        numberOf7s++;
        break;
      case 8:
        System.out.print("The 8 of ");
        numberOf8s++;
        break;
      case 9:
        System.out.print("The 9 of ");
        numberOf9s++;
        break;
      case 10:
        System.out.print("The 10 of ");
        numberOf10s++;
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myCard5/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //below finds out whether we have a pair of cards and adds to the counter numberOfPairs if it finds a pair
    int numberOfPairs=0;
    if(numberOfAces>1) {
      numberOfPairs+=(numberOfAces/2);
    }
    if(numberOf2s>1) {
      numberOfPairs+=(numberOf2s/2);;
    }
    if(numberOf3s>1) {
      numberOfPairs+=(numberOf3s/2);;
    }
    if(numberOf4s>1) {
      numberOfPairs+=(numberOf4s/2);;
    }
    if(numberOf5s>1) {
      numberOfPairs+=(numberOf5s/2);;
    }
    if(numberOf6s>1) {
      numberOfPairs+=(numberOf6s/2);;
    }
    if(numberOf7s>1) {
      numberOfPairs+=(numberOf7s/2);;
    }
    if(numberOf8s>1) {
      numberOfPairs+=(numberOf8s/2);;
    }
    if(numberOf9s>1) {
      numberOfPairs+=(numberOf9s/2);;
    }
    if(numberOf10s>1) {
      numberOfPairs+=(numberOf10s/2);;
    }
    if(numberOfJacks>1) {
      numberOfPairs+=(numberOfJacks/2);;
    }
    if(numberOfQueens>1) {
      numberOfPairs+=(numberOfQueens/2);;
    }
    if(numberOfKings>1) {
      numberOfPairs+=(numberOfKings/2);;
    }
    
    //Below checks if I have three or more of any one type of card (Checking for three of a kind)
    boolean threeOfAKind=(numberOfAces>=3||numberOf2s>=3||numberOf3s>=3||numberOf4s>=3||numberOf5s>=3||numberOf6s>=3
    ||numberOf7s>=3||numberOf8s>=3||numberOf9s>=3||numberOf10s>=3||numberOfJacks>=3||numberOfQueens>=3||numberOfKings>=3);
    
    //Below finds out if you have a three of a kind and prints out if you do, 
    //then checks if you don't checks for two pair, pair and finally just high card
    if(threeOfAKind) {
      System.out.println("You have three of a kind.");
    } else if(numberOfPairs==2) {
      System.out.println("You have two pairs.");
    } else if(numberOfPairs==1) {
      System.out.println("You have a pair.");
    } else {
      System.out.println("You have a high card.");
    }
    
  }
}
