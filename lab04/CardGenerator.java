//This randomly prints a card in a full deck of cards
//import java.lang.Math;
public class CardGenerator{
  
  public static void main(String args[]) {
    //Below creates a random number from 1-52 as there are 52 cards in a deck
    int myRandomCard=(int)(Math.random()*52)+1;
    
    //Below figures out what number or name of the card and prints out the number or name i.e. "You picked the Queen of"
    switch(myRandomCard%13) {
      case 0: 
        System.out.print("You picked the King of ");
        break;
      case 1:
        System.out.print("You picked the Ace of ");
        break;
      case 11:
        System.out.print("You picked the Jack of ");
        break;
      case 12:
        System.out.print("You picked the Queen of ");
        break;
      default:
        System.out.print("You picked the "+(myRandomCard%13)+" of ");
    }
    
    //Below divides by 13 to figure out which suit the card is from
    switch(myRandomCard/13) {
      case 0:
        System.out.println("Diamonds.");
        break;
      case 1:
        System.out.println("Clubs.");
        break;
      case 2:
        System.out.println("Hearts.");
        break;
      case 3:
        System.out.println("Spades.");
    }
    
    //Below prints the card number generated so that way I can confirm the program works correctly. Commented out after testing
    //System.out.println("Card number: "+myRandomCard);
  }
}