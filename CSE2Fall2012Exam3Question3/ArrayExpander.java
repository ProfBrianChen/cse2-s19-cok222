//This is a class made as a solution to the fall 2012 exam 3 question 3

public class ArrayExpander {
  
  public static void main(String args[]) {
    
  }
  
  public static int[] expand(int[] in) {
    int[] out = new int[in.length*2];
    
    for(int currentIndex=0; currentIndex<in.length;currentIndex++) {
      out[currentIndex] = in[currentIndex];
    }
    
    return out;
  }
}