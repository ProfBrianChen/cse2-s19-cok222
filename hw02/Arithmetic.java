//Calculates total cost of each input item, each items sales tax, total cost pre-tax, total tax, and total bill
//Outputs the above calculations to the terminal
public class Arithmetic {
  public static void main(String args[]) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //initializing all of my calculated values I want to print to the user
    double costOfPants, costOfShirts, costOfBelts, taxOnPants, taxOnShirts, taxOnBelts, costOfTax, costBeforeTax, costAfterTax;
    
    //Calculating cost using number of items times the price of the item
    costOfPants=pantsPrice*numPants;
    costOfShirts=numShirts*shirtPrice;
    costOfBelts=numBelts*beltCost;
    
    //Calculating tax of each item using the cost of each time times the sales tax
    taxOnPants=costOfPants*paSalesTax;
    taxOnShirts=costOfShirts*paSalesTax;
    taxOnBelts=costOfBelts*paSalesTax;
    
    //calculating total cost of tax by summing taxs per an item together
    costOfTax=taxOnBelts+taxOnShirts+taxOnPants;
    
    //calculating total cost of items pre-tax by summing total costs
    costBeforeTax=costOfBelts+costOfPants+costOfShirts;
    
    //calculating total cost after tax using cost before tax and cost of tax
    costAfterTax=costBeforeTax+costOfTax;
    
    //Below Prints out the data calculated above
    System.out.println("Cost of Pants is: $"+costOfPants);
    System.out.println("Cost of Belts is: $"+costOfBelts);
    System.out.println("Cost of Shirts is: $"+costOfShirts);
    System.out.println("Cost of tax on Pants is: $"+String.format("%.2f",taxOnPants));
    System.out.println("Cost of tax on Belts is: $"+String.format("%.2f",taxOnBelts));
    System.out.println("Cost of tax on Shirts is: $"+String.format("%.2f",taxOnShirts));
    System.out.println("Cost of Tax is: $"+String.format("%.2f", costOfTax));
    System.out.println("Total cost before tax is: $"+costBeforeTax);
    System.out.println("Total cost with tax is: $"+String.format("%.2f",costAfterTax));
  }
}